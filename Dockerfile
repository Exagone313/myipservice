FROM docker.io/golang:1.18-alpine as builder
WORKDIR /src
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o myipservice .

FROM scratch
COPY --from=builder /src/myipservice /bin/
ENV PATH=/bin
ENTRYPOINT ["/bin/myipservice"]
