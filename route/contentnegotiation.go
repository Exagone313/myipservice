package route

import (
	"net/http"
	"strings"
)

func ContentNegotiation(writer http.ResponseWriter, request *http.Request) {
	accept := request.Header.Get("Accept")
	if strings.Contains(accept, "application/json") {
		Json(writer, request)
		return
	}
	Plain(writer, request)
}
