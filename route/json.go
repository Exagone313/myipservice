package route

import (
	"encoding/json"
	"net/http"

	"gitlab.com/Exagone313/myipservice/requestdata"
)

func Json(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Content-Type", "application/json")
	data := requestdata.LoadRequestData(request)
	result, err := json.Marshal(data)
	if err != nil {
		writer.WriteHeader(http.StatusInternalServerError)
		_, _ = writer.Write([]byte("{\"error\":\"internal\"}"))
	} else {
		writer.Write(result)
	}
}
