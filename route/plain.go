package route

import (
	"fmt"
	"net/http"

	"gitlab.com/Exagone313/myipservice/requestdata"
)

func Plain(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Content-Type", "text/plain; charset=utf-8")
	data := requestdata.LoadRequestData(request)
	fmt.Fprintf(writer, "Time: %s\n", data.DateTime)
	fmt.Fprintf(writer, "Remote: %s\n", data.RemoteAddress)
	if data.GeoIPCountry != "" {
		fmt.Fprintf(writer, "GeoIP Country: %s (%s)\n",
			data.GeoIPCountry, data.GeoIPCountryISO)
	}
	if data.GeoIPCity != "" {
		fmt.Fprintf(writer, "GeoIP City: %s (%.4f:%.4f)\n",
			data.GeoIPCity, data.GeoIPCityLatitude, data.GeoIPCityLongitude)
	}
	if data.TLSVersion != "" {
		fmt.Fprintf(writer, "TLS: %s\n", data.TLSVersion)
	}
	if data.TLSCipher != "" {
		fmt.Fprintf(writer, "Cipher: %s\n", data.TLSCipher)
	}
	fmt.Fprintf(writer, "HTTP: %s\n", data.HTTPVersion)
	if data.HTTPUserAgent != "" {
		fmt.Fprintf(writer, "User agent: %s\n", data.HTTPUserAgent)
	}
}
