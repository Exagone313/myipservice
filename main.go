package main

import (
	"log"
	"net/http"

	"gitlab.com/Exagone313/myipservice/route"
	"gitlab.com/Exagone313/myipservice/util"
)

func main() {
	http.Handle("/", route.CORSMiddleware(http.HandlerFunc(
		route.ContentNegotiation)))
	http.Handle("/index.json", route.CORSMiddleware(http.HandlerFunc(
		route.Json)))
	http.Handle("/index.txt", route.CORSMiddleware(http.HandlerFunc(
		route.Plain)))

	listen := util.Getenv("LISTEN", ":8080")
	log.Println("Listening on", listen)
	http.ListenAndServe(listen, nil)
}
