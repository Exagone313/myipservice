# My IP Service

My IP Service is a microservice that returns data associated with your HTTP request.

## Usage

Requires Go 1.17+ installed.

```
make
./myipservice
```

### Environment Variables

You can set environment variables to fetch some values from custom HTTP headers.
The values of these variables will be used as HTTP header names (case insensitive).
* `HTTP_REAL_IP` (instead of request remote address, usually `X-Real-IP`)
* `HTTP_TLS_VERSION`
* `HTTP_TLS_CIPHER`
* `HTTP_VERSION`

You can also set `LISTEN` to change the listening address and port (default `:8080`),
following the format for `ListenAndServe` function from the [net/http package](https://godocs.io/net/http).

You can set `GEOIP_CITY` to a path to a GeoIPv2 City database (e.g. `GeoLite2-City.mmdb`).

### Docker

A Docker image is available at: `registry.ewd.app/exagone313/myipservice`.

## License

See [COPYING](COPYING).
