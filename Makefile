BIN ?= myipservice

.PHONY: all
all: $(BIN)

$(BIN): $(shell find . -type f -name '*.go')
	go build -o $@ .

.PHONY: clean
clean:
	$(RM) $(BIN)
