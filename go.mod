module gitlab.com/Exagone313/myipservice

go 1.18

require github.com/oschwald/geoip2-golang v1.7.0

require (
	github.com/oschwald/maxminddb-golang v1.9.0 // indirect
	golang.org/x/sys v0.0.0-20220520151302-bc2c85ada10a // indirect
)
