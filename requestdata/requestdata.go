package requestdata

import (
	"net"
	"net/http"
	"os"
	"time"

	geoip2 "github.com/oschwald/geoip2-golang"
)

type RequestData struct {
	DateTime string `json:"date_time"`
	RemoteAddress string `json:"remote_address"`
	TLSVersion string `json:"tls_version,omitempty"`
	TLSCipher string `json:"tls_cipher,omitempty"`
	HTTPVersion string `json:"http_version"`
	HTTPUserAgent string `json:"http_user_agent,omitempty"`
	GeoIPCountry string `json:"geoip_country,omitempty"`
	GeoIPCountryISO string `json:"geoip_country_iso,omitempty"`
	GeoIPCity string `json:"geoip_city,omitempty"`
	GeoIPCityLatitude float64 `json:"geoip_city_latitude,omitempty"`
	GeoIPCityLongitude float64 `json:"geoip_city_longitude,omitempty"`
}

func getValueFromHeader(request *http.Request, env string, defaultValue string) string {
	header := os.Getenv(env)
	if len(header) > 0 {
		return request.Header.Get(header)
	}
	return defaultValue
}

func LoadRequestData(request *http.Request) (data *RequestData) {
	data = &RequestData{
		DateTime: time.Now().UTC().Format(time.RFC3339),
		RemoteAddress: getValueFromHeader(request, "HTTP_REAL_IP", request.RemoteAddr),
		TLSVersion: getValueFromHeader(request, "HTTP_TLS_VERSION", ""),
		TLSCipher: getValueFromHeader(request, "HTTP_TLS_CIPHER", ""),
		HTTPVersion: getValueFromHeader(request, "HTTP_VERSION", request.Proto),
		HTTPUserAgent: request.Header.Get("User-Agent"),
	}
	loadGeoIPCity(request, data)
	return
}

func loadGeoIPCity(request *http.Request, data *RequestData) {
	if len(data.RemoteAddress) == 0 {
		return
	}
	geoip_city_path := os.Getenv("GEOIP_CITY")
	if len(geoip_city_path) == 0 {
		return
	}
	db, err := geoip2.Open(geoip_city_path)
	if err != nil {
		return
	}
	defer db.Close()
	record, err := db.City(net.ParseIP(data.RemoteAddress))
	if err != nil {
		return
	}
	data.GeoIPCountry = record.Country.Names["en"]
	data.GeoIPCountryISO = record.Country.IsoCode
	data.GeoIPCity = record.City.Names["en"]
	data.GeoIPCityLatitude = record.Location.Latitude
	data.GeoIPCityLongitude = record.Location.Longitude
}
